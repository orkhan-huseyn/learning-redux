import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from './store/actions/counter';
import { login, logout } from './store/actions/user';

export default function App() {
  const counter = useSelector((state) => state.counter.value);
  const user = useSelector((state) => state.user.authUser);
  const dispatch = useDispatch();

  return (
    <div>
      <h1>Counter is {counter}!</h1>
      <button onClick={() => dispatch(increment(2))}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <hr />
      <div>{JSON.stringify(user)}</div>
      <button onClick={() => dispatch(login())}>Login</button>
      <button onClick={() => dispatch(logout())}>Logout</button>
    </div>
  );
}
