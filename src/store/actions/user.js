export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';

export function login() {
  return async (dispatch) => {
    const response = await fetch('https://dummyjson.com/auth/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: 'kminchelle',
        password: '0lelplR',
      }),
    });
    const user = await response.json();
    dispatch(setUser(user));
  };
}

export function setUser(user) {
  return {
    type: LOGIN_USER,
    payload: user,
  };
}

export function logout() {
  return {
    type: LOGOUT_USER,
  };
}
