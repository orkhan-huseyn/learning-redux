export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const RESET = 'RESET';

// action creator
export function increment(step) {
  return {
    type: INCREMENT,
    payload: step,
  };
}

export function decrement() {
  return {
    type: DECREMENT,
  };
}
