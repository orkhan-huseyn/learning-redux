import { LOGIN_USER, LOGOUT_USER } from '../actions/user';

const initialState = {
  authUser: null,
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER:
      return {
        authUser: action.payload,
      };
    case LOGOUT_USER:
      return {
        authUser: null,
      };
    default:
      return state;
  }
}

export default userReducer;
